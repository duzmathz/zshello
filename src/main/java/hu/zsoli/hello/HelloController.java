package hu.zsoli.hello;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.metrics.annotation.Timed;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Timed
public class HelloController {

    @Value("${TARGET:World}")
    private String target;
    
    @RequestMapping("/")
    public String hello() {
        return "Hello " +  target;
    }

}

